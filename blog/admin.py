from django.contrib import admin

from django_summernote.admin import SummernoteModelAdmin
from .models import Tag, Article
# Register your models here.


def make_action(modeladmin, request, queryset):
    queryset.update(active=True)

def make_action_unselect(modeladmin, request, queryset):
    queryset.update(active=False)

make_action.short_description = "Отметить как опубликованые"
make_action_unselect.short_description = "Отметить как не опубликованые"


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    pass


@admin.register(Article)
class ArticleAdmin(SummernoteModelAdmin, admin.ModelAdmin):
    list_display = ('title', 'create', 'update', 'active', )
    prepopulated_fields = {'slug': ('title', )}
    search_fields = ('title', )
    ordering = ('-update', )
    actions = (make_action, make_action_unselect, )

    summernote_fields = ('description',)