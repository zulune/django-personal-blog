from django.db import models
from django.urls import reverse
# Create your models here.


class Tag(models.Model):
    name = models.CharField(max_length=120)

    def __str__(self):
        return self.name


class Article(models.Model):
    title = models.CharField(max_length=120)
    slug = models.SlugField(unique=True)
    image = models.ImageField(upload_to='blog/article/')
    description = models.TextField()
    active = models.BooleanField(default=True)
    tag = models.ManyToManyField(Tag)
    create = models.DateTimeField(auto_now_add=True)
    update = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('blog:article-detail', kwargs={'slug': self.slug})