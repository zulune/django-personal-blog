from django.shortcuts import render
from django.views.generic import ListView, DetailView

from .models import Tag, Article
# Create your views here.


class ArticleListView(ListView):
    model = Article
    template_name = 'blog/articles.html'
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(ArticleListView, self).get_context_data(**kwargs)
        tags = Tag.objects.all()
        context['tags'] = tags
        return context



class ArticleDetailView(DetailView):
    template_name = 'blog/article.html'
    model = Article