from django.db import models
from django.urls import reverse
# Create your models here.


class Student(models.Model):
    first_name = models.CharField(max_length=120)
    last_name = models.CharField(max_length=120)

    def __str__(self):
        return "{} {}".format(self.first_name, self.last_name)


class Portfolio(models.Model):
    name = models.CharField(max_length=120)
    slug = models.SlugField(unique=True)
    skils = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('mysite:portfolio-detail', kwargs={'slug': self.slug})


class Contact(models.Model):
    email = models.EmailField()
    subject = models.CharField(max_length=120)
    message = models.TextField()

    def __str__(self):
        return self.email


class SocialLinks(models.Model):
    name = models.CharField(max_length=120)
    link = models.URLField()
    icons = models.ImageField(
        upload_to='images/SocialLinks'
    )
    active = models.BooleanField(default=True)
    order_by = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.name