from mysite.models import SocialLinks
from mysite.utils import memoize


@memoize
def social_links(request):
    return {
        'social_icons': SocialLinks.objects.filter(active=True).order_by('order_by')
    }