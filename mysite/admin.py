from django.contrib import admin
from django.utils.html import format_html

from django_summernote.admin import SummernoteModelAdmin

from .models import (
    Student, 
    Contact, 
    Portfolio, 
    SocialLinks
)
# Register your models here.


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    pass


@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):
    pass


@admin.register(Portfolio)
class PortfolioAdmin(SummernoteModelAdmin, admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}
    summernote_fields = ('description', )


@admin.register(SocialLinks)
class SocialLinksAdmin(admin.ModelAdmin):
    list_display = ('image_tag', 'name', 'link', 'order_by', 'active', )
    list_editable = ('order_by',)
    list_display_links = ('name', )

    def image_tag(self, obj):
        return format_html(
            '<img src="{}" / width=30>'.format(obj.icons.url)
        )

    image_tag.short_description = 'Icons'
