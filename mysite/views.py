import json
import requests
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect

from .models import Contact, Portfolio
# Create your views here.


def index(request):
    return render(request, 'mysite/index.html', {})


def contact(request):
    if request.method == 'POST':
        r_email = request.POST.get('email')
        r_subject = request.POST.get('subject')
        r_message = request.POST.get('message')

        c = Contact(email=r_email, subject=r_subject, message=r_message)
        c.save()
        return render(request, 'mysite/thank.html', {})
    else:
        return render(request, 'mysite/contact.html', {})


def portfolio(request):
    portfolio = Portfolio.objects.all()
    template_data = {"portfolio_all": portfolio}
    return render(request, 'mysite/portfolio.html', template_data)


def portfolio_detail(request, slug):
    instance = Portfolio.objects.get(slug=slug)
    template_data = {'instance': instance}
    return render(request, 'mysite/portfolio-detail.html', template_data)