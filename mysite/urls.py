from django.urls import path

from .views import (
    index,
    contact,
    portfolio,
    portfolio_detail
)


app_name = 'mysite'


urlpatterns = [
    path('', index, name="home"),
    path('contact', contact, name="contact"),
    path('portfolio', portfolio, name="portfolio"),
    path('portfolio/<slug>', portfolio_detail, name="portfolio-detail"),
]