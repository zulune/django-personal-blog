# Generated by Django 2.1.4 on 2018-12-19 19:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mysite', '0006_sociallinks'),
    ]

    operations = [
        migrations.AddField(
            model_name='sociallinks',
            name='order_by',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
